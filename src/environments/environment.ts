// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,

  firebaseConfig: {
    apiKey: "AIzaSyAWakhXa87sSk1YaRcVt1qUjZx0hd8xgac",
    authDomain: "claaaaas.firebaseapp.com",
    databaseURL: "https://claaaaas.firebaseio.com",
    projectId: "claaaaas",
    storageBucket: "claaaaas.appspot.com",
    messagingSenderId: "550359331240",
    appId: "1:550359331240:web:d02294dc9c45f5a0696d9d",
    measurementId: "G-9VBGF7JWS7"
  }


/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
};